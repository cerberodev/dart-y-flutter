# Temas puntuales de la sección

## Resumen:

### En esta sección veremos todo lo relacionado a las funciones en Dart y formas de comprender sus argumentos.

## Puntualmente:

- Funciones básicas

- Argumentos

- Argumentos por valor y referencia

- Funciones Lambda o funciones de Flecha

- Callbacks

#### Al final hay una tarea para practicar las funciones y afianzar los conocimientos.