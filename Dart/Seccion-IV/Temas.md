# Temas puntuales de la sección

## Resumen de la sección:

#### Aquí vamos a tratar temas sobre el control de flujo de un programa en Dart, aprenderemos a tomar decisiones y usar ciclos (loops) para poder repetir o controlar cómo queremos que la aplicación se ejecute. Los temas puntuales son:

- If y else

- Ciclo For

- For In

- While

- Do While

-  Break y Continue

- Switch

##### Al final de la misma, tenemos un examen para afianzar los conocimientos