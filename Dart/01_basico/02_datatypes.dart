main(){

    // Numeros
      //var a = 10;
      //a= 20;
      //print(a);
      int a = 10;
      double b = 10.5;
      int c = 1;

      double x = 10, y = 20, z = 30;


      //print( a + b + c);
      //print( x );
      //print( y );
      //print( z );





    // Strings - cadenas de caracteres


    String nombre     = 'Pierre';
    String apellido   = "Guillen";
    String nomape     = 'Rocky\'S'; //Rocky'S 

    String multilinea = '''
    Hola Mundo
    Como estan?
  ''';

    //print(multilinea);





    // Booleans

    bool activo = true;
    bool corriendo;

    //activo = !activo; (Negacion)

    //print(activo);




    // Listas - Arreglos

    var personajes = ['Superman', 'Batman'];            //List<String>
    List<String> personajes3 = ['Superman', 'Batman'];  // No deja añadir numeros
    var personajes2 = ['Superman', 'Batman', 96, true]; //List<Object>

    List<String> personajes4 = new List();
    personajes4.add('Superman');
    personajes4.add('Batman');

    List<String> personajes5 = new List();
    personajes5.addAll(['Superman', 'Batman', 'Robin']);

    List<String> personajes6 = new List();
    personajes6..add('Batman')
               ..add('Superman')
               ..add('Batman');

    List<String> villanos = new List(3);
    //villanos.addAll(['Lex', 'Red Skull', 'Doom']);
    villanos[0] = 'Lex';
    villanos[1] = 'Red Skull';
    villanos[2] = 'Doom';
    //print(personajes6);    

    // Sets

    Set<String> villanos2 = { 'Lex', 'Red Skull', 'Doom'};

    villanos2.add('Lex');

    //print(villanos2);


    // Mapas - Diccionarios - Objetos
    // llave : valor
    Map<dynamic, String> iroman = {
      'nombre'  : 'Tony Staark',
      'poder'   : 'Inteligencia y el dinero',
      //'edad'    : 60
      10        : 'Nivel de energia'
    };

    //print(iroman);

    Map<String, dynamic> capitan = new Map();

    capitan.addAll({'nombre': 'Steve', 'poder': 'Sportar suero sin morir'});

    print(capitan);






}