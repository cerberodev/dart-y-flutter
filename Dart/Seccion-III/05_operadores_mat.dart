/**
 * Un operador es un simbolo que le dice al compilador
 * que debe realizar una tarea matematica, relacionala
 * o logica y debe de producir un resultado.
 * [Creado] : Miercoles 04/09/2019
 */

main(){

  int a = 96 + 4;     // +  100
  a = 20 - 10;        // -  10
  a = 10 *2;          // *  20

  double b = 10 / 2;  //  / 5
  b = 10.0 % 2;       //  % 1 El sobrante de la division.
  b = -b;             //  -expresion  Es usado para cambiar el valor de la exprecion

  int c = 10 ~/ 3;    //  ~/  3 Division comun y corriente (solo parte entera de la division)


  int d = 1;

  d++;                //  ++  2 (Incrementa en 1 el valor)
  d--;                //  --  1

  d +=  2;            //  +=  3
  d -=  2;            //  -=  1 *=  /=


}