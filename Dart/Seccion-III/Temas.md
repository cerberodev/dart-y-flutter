#Temas:

###Esta es una de las secciones más cortas del curso, pero es muy importante, aquí aprenderemos sobre:

- Variables

- Constantes vs Finals

- Operadores Aritméticos

- Operadores de Asignación

- Operadores condicionales

- Operadores relacionales

####Al final, también tendremos un examen teórico para reforzar lo aprendido